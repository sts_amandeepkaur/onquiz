# Generated by Django 2.2.1 on 2019-07-15 03:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Quiz', '0010_delete_apt2'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='registerds',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AlterField(
            model_name='contact',
            name='added_on',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
