# Generated by Django 2.2.1 on 2019-07-18 03:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Quiz', '0029_auto_20190718_0826'),
    ]

    operations = [
        migrations.AlterField(
            model_name='apt',
            name='test',
            field=models.CharField(choices=[('Test1', 'Test1'), ('Test2', 'Test2'), ('Test3', 'Test3'), ('Test4', 'Test4')], max_length=30),
        ),
    ]
