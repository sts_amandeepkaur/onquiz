# Generated by Django 2.2.1 on 2019-07-13 13:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Quiz', '0006_auto_20190713_1839'),
    ]

    operations = [
        migrations.CreateModel(
            name='apt2',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('s_no', models.IntegerField()),
                ('ques', models.CharField(max_length=250)),
                ('opt1', models.CharField(max_length=50)),
                ('opt2', models.CharField(max_length=50)),
                ('opt3', models.CharField(max_length=50)),
                ('opt4', models.CharField(max_length=50)),
                ('corr', models.CharField(max_length=50)),
                ('tst', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Quiz.apt')),
            ],
        ),
        migrations.CreateModel(
            name='apt1',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('s_no', models.IntegerField()),
                ('ques', models.CharField(max_length=250)),
                ('opt1', models.CharField(max_length=50)),
                ('opt2', models.CharField(max_length=50)),
                ('opt3', models.CharField(max_length=50)),
                ('opt4', models.CharField(max_length=50)),
                ('corr', models.CharField(max_length=50)),
                ('tst', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Quiz.apt')),
            ],
        ),
    ]
