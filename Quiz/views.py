from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from .models import contact,user,qs,group,apt
from django.contrib.auth.models import User
from django.contrib.auth import login,logout,authenticate
from django.contrib.auth.decorators import login_required

al=group.objects.all().order_by('prg')

def first(r):
    
    if r.method=="POST":

        
        if "signup" in r.POST:
            fn=r.POST['f_nm']
            ln=r.POST['l_nm']
            ph=r.POST['mb']
            em=r.POST['em']
            pss=r.POST['pss']
            dd=r.POST['dd']
            mm=r.POST['mm']
            yy=r.POST['yy']
            sex=r.POST['sex']

            usr = User.objects.create_user(em,em,pss)
            usr.first_name=fn
            usr.last_name=ln
            usr.save()

            us=user(usr=usr,contact=ph,Gender=sex,dob=yy+"-"+mm+"-"+dd)
            us.save()
            if "image" in r.FILES:
                us.image = r.FILES['image']
                us.save()
            
            rs="Dear {} thanks for Sign Up!!".format(fn+ln);
            nm=fn+ln;
            return render(r,'home.html',{'result':rs,'nm':nm})
        elif 'signin' in r.POST:
            em_ph=r.POST['ph_em']
            ps=r.POST['password']
            check = authenticate(username=em_ph,password=ps)
            if check:
                usr=User.objects.get(username=em_ph)
                nm=usr.first_name+" "+usr.last_name;
                login(r,check)
                rs="Welcome Dear {}!!".format(nm);
                return HttpResponseRedirect('/')
            else:
                return render(r,'first.html',{'st':'Incorrect username & password'})
    return render(r,'first.html')

def home(r):
    profile=prof(r)
    rs=contactpage(r)
    return  render(r,'home.html',{'result':rs,'cat':al,'profile':profile})
    
def online(r):
    profile=prof(r)
    rs=contactpage(r)
    return render(r,'Online.html',{'result':rs,'cat':al,'profile':profile})

def aptitudetest(r):
    profile=prof(r)
    rs=contactpage(r)
    if 'id' in r.GET:
        id = r.GET['id']
        data = apt.objects.filter(cat__id=id)
        return render(r,'aptitudetest.html',{'result':rs,'s':data,'cat':al,'profile':profile})
    # return render(r,'aptitudetest.html',{'result':rs,})

def instastrtest(r):
    profile=prof(r)
    return render(r,'instastrtest.html',{'result':rs,'cat':al})


def startapptest1(r):
    profile=prof(r)
    right,wrong,un=0,0,0
    if 'id' in r.GET:
        id=r.GET['id']
        ids=r.GET['type']
        data=qs.objects.filter(cat__id=id,tst=ids)
        if r.method=="POST":
            if "contact" in r.POST:
                rs=contactpage(r)
                return render(r,'startapptest1.html',{'result':rs,'s':data,'cat':al,'profile':profile})
    
            if 'su' in r.POST:
                mydata = []
                for j in data:
                    if r.POST['q'+str(j.s_no)]==r.POST['answer'+str(j.s_no)]:
                        right += 1
                        abcd = {'q':j.ques,'op1':j.opt1,'op2':j.opt2,'op3':j.opt3,'op4':j.opt4, 'color':'text-success', 'useranswer':r.POST['q'+str(j.s_no)],'correct':j.corr,'co':j}
                        mydata.append(abcd)
                    # elif r.POST['q'+str(j.s_no)]=="":
                    #     abcd = {'q':j.ques,'op1':j.opt1,'op2':j.opt2,'op3':j.opt3,'op4':j.opt4, 'color':'text-primary', 'useranswer':'e','correct':j.corr,'co':j}
                    #     mydata.append(abcd)
                    #     un +=1
                    else:
                        wrong += 1
                        abcd = {'q':j.ques,'op1':j.opt1,'op2':j.opt2,'op3':j.opt3,'op4':j.opt4, 'color':'text-danger', 'useranswer':r.POST['q'+str(j.s_no)],'correct':j.corr,'co':j}
                        mydata.append(abcd)

                        
                rs = {'right':right,'wrong':wrong,'total':len(data)}
                
                return render(r,'result.html',{'s':mydata,'cat':al,'rs':rs,'d':data,'profile':profile})
        return render(r,'startapptest1.html',{'s':data,'cat':al,'profile':profile})
    return render(r,'startapptest1.html')

@login_required
def result(r):
    profile=prof(r)
    return render(r,'result.html',{'profile':profile})


def aptitude(r):
    profile=prof(r)
    rs=contactpage(r)
    return render(r,'aptitude.html',{'result':rs,'cat':al,'profile':profile})
def bank(r):
    profile=prof(r)
    rs=contactpage(r)
    return render(r,'Bank.html',{'result':rs,'cat':al,'profile':profile})
def engg(r):
    profile=prof(r)
    rs=contactpage(r)
    return render(r,'Engineering.html',{'result':rs,'cat':al,'profile':profile})
def eng(r):
    profile=prof(r)
    rs=contactpage(r)
    return render(r,'English.html',{'result':rs,'cat':al,'profile':profile})
def gk(r):
    profile=prof(r)
    rs=contactpage(r)
    return render(r,'GK.html',{'result':rs,'cat':al,'profile':profile})
def interview(r):
    profile=prof(r)
    rs=contactpage(r)
    return render(r,'interview.html',{'result':rs,'cat':al,'profile':profile})
def placement(r):
    profile=prof(r)
    rs=contactpage(r)
    return render(r,'Placement.html',{'result':rs,'cat':al,'profile':profile})
def reasoning(r):
    profile=prof(r)
    rs=contactpage(r)
    return render(r,'reasoning.html',{'result':rs,'cat':al,'profile':profile})

def contactpage(r):
    if r.method=="POST":
        n = r.POST['name']
        em = r.POST['email']
        cn = r.POST['ph']
        msz = r.POST['msg']

        data = contact(name=n,email=em,contact=cn,message=msz)
        data.save()
        rs="Dear {} thanks for your time!!".format(n);
        return rs;
def check_user(request):
    un = request.GET['usern']
    ch = User.objects.filter(username=un)
    if len(ch)<1:
        return HttpResponse(0)
    else:
        return HttpResponse(1)

@login_required
def uslogout(request):
    logout(request)
    return HttpResponseRedirect('Register/SignIn')

def prof(r):
    profile={}
    if r.user.is_authenticated:
        if not r.user.is_superuser:
            profile = user.objects.get(usr__id=r.user.id)
            return profile;
