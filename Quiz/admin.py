from django.contrib import admin
from .models import contact,user,group,qs,apt
admin.site.site_header = "OnQuiz"

class groupAdmin(admin.ModelAdmin):
    fields=['s_no','prg']
    list_display=['s_no','prg','id']
    list_filter=['s_no','prg']
    list_editable=['prg']
    search_fields=['s_no','prg']

class contactAdmin(admin.ModelAdmin):
    fields=['name','contact','email']
    list_display=['name','email','contact','added_on']
    list_filter=['name','email']
    search_fields=['name','email','contact']

class qsAdmin(admin.ModelAdmin):
    list_display=['id','cat','tst']

class aptAdmin(admin.ModelAdmin):
    list_display=['id','cat','test']

admin.site.register(contact,contactAdmin)
admin.site.register(user)
admin.site.register(group,groupAdmin)
admin.site.register(apt,aptAdmin)
admin.site.register(qs,qsAdmin)
