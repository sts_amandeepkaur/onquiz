from django.db import models
from django.contrib.auth.models import User


class contact(models.Model):
    name = models.CharField(max_length=250)
    email = models.EmailField()
    contact = models.IntegerField()
    message = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    registerds=models.DateTimeField(auto_now_add=True,null=True)
    def __str__(self):
        return self.name

class user(models.Model):
    usr = models.OneToOneField(User,on_delete=models.CASCADE)
    contact=models.IntegerField(null=True)
    dob=models.DateField()
    registerd=models.DateTimeField(auto_now_add=True)
    Gender=models.CharField(max_length=20)
    image=models.ImageField(upload_to='image/profile',null=True)

    def __str__(r):
        return r.usr.username
    
class group(models.Model):
    
    s_no=models.IntegerField()
    prg=models.CharField(max_length=100)
    # tst=models.CharField(choices=c,max_length=30)

    def __str__(r):
        return r.prg

class test(models.Model):
    t=models.CharField(max_length=30)

    def __str__(r):
        return r.t

class apt(models.Model):
    c=(
        ('Test1','Test1'),('Test2','Test2',),
        ('Test3','Test3'),('Test4','Test4',)
    )
    cat=models.ForeignKey(group,on_delete=models.CASCADE)
    test=models.CharField(choices=c,max_length=30)

    def __str__(r):
        return r.test

class qs(models.Model):
    c=(
        ('Test1','Test1'),('Test2','Test2',),
        ('Test3','Test3'),('Test4','Test4',)
    )
    cat=models.ForeignKey(group,on_delete=models.CASCADE)
    tst=models.CharField(choices=c,max_length=30)
    s_no=models.IntegerField()
    ques=models.CharField(max_length=250)
    opt1=models.CharField(max_length=50)
    opt2=models.CharField(max_length=50)
    opt3=models.CharField(max_length=50)
    opt4=models.CharField(max_length=50)
    corr=models.CharField(max_length=50)

    def __str__(r):
        return str(r.s_no)  
