"""OnQuiz URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Quiz import views

from django.conf import settings #import media
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('Register/SignIn',views.first ,name="first"),
    path('check_user/',views.check_user,name="check_user"),
    path('uslogout',views.uslogout,name="uslogout"),
    
    path('',views.home ,name="home"),
    path('online/',views.online, name="online"),
    path('aptitude/',views.aptitude, name="aptitude"),
    path('bank/',views.bank, name="bank"),
    path('engineering/',views.engg, name="engg"),
    path('english/',views.eng, name="eng"),
    path('gk/',views.gk, name="gk"),
    path('interview/',views.interview, name="interview"),
    path('placement/',views.placement, name="placement"),
    path('reasoning/',views.reasoning, name="reasoning"),

    path('online/aptitudetest/',views.aptitudetest, name="aptitudetest" ),
    path('online/aptitudetest/instastrtest/',views.instastrtest, name="instastrtest"),
    path('online/aptitudetest/startapptest1',views.startapptest1, name="startapptest1"),
    path('online/test/result',views.result, name="result"),
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
